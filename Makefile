
build:
	docker build -t gitlabcidind .

run:
	docker rm -f -v build; docker run --name build -i gitlabcidind:latest

bash:
	docker rm -f -v build; docker run --name build -i gitlabcidind:latest /bin/bash